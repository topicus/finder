#!/bin/sh

FINDER_FILE="$HOME/bin/finder"

if [ ! -d "$HOME/bin" ];then
	mkdir "$HOME/bin"
fi
curl --progress-bar "http://topicus.webfactional.com/static/finder/finder" > "$FINDER_FILE" && chmod 0755 "$FINDER_FILE" 
trap "rm $FINDER_FILE; exit" SIGHUP SIGINT SIGTERM

if [ ! -f "$HOME/.bash_profile" ];then
	touch "$HOME/.bash_profile"
fi
. "$HOME/.bash_profile"
if [ $(echo  $PATH | grep -c $HOME/bin) -lt 1 ];then
	export "PATH=$PATH:$HOME/bin"
	#add path if it's not added yet
	grep -q 'export PATH=$PATH:$HOME/bin' ~/.bash_profile || echo 'export PATH=$PATH:$HOME/bin' >>  ~/.bash_profile
fi
echo 'DONE!'
echo 'Usage: finder [path] [pattern]'