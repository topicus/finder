Finder
================================
Is a tool like ack, just made it for learning purposes

Install
--------------------------------
    curl -s http://topicus.webfactional.com/static/finder/install.sh | /bin/bash && export PATH=$PATH:$HOME/bin/

Usage
--------------------------------
    finder [path] [string]

Examples
--------------------------------
    finder ~/Projects 